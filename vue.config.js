const UglifyJsPlugin = require('uglifyjs-webpack-plugin');

module.exports = {
  publicPath: process.env.NODE_ENV === 'production'
    ? '/themes/rognikita-b4/assets/openweather/'
    : '/',
  configureWebpack: {
    optimization: {
      minimizer: [
        new UglifyJsPlugin({
          extractComments: 'all',
        }),
      ],
    },
  },
  // chainWebpack: config => config.optimization.minimize(false),
  css: {
    requireModuleExtension: true,
    loaderOptions: {
      css: {
        modules: {
          localIdentName: process.env.NODE_ENV === 'production'
            ? '[hash:base64:4]'
            : '[name]__[local]_[hash:base64:4]',
        },
      },
    },
  },
};
