import Vue from 'vue';
import App from './App';
import router from './router';

import '@/theme/main.scss';
import 'vue-scrollable-container2/vscrollbar.css';

Vue.config.productionTip = false;

new Vue({
  router,
  render: (h) => h(App),
}).$mount('#app');
