import axios from 'axios';

const extraAxios = axios.create();

extraAxios.interceptors.request.use(
  (config) => config,
  (error) => Promise.reject(error)
);

export default extraAxios;
