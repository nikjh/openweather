export const getStorageItem = (key) => {
  const item = window.localStorage.getItem(key);
  return item ? JSON.parse(item) : null;
};

export const setStorageItem = (key, value) => {
  const item = JSON.stringify(value);
  window.localStorage.setItem(key, item);
};

export const removeStorageItem = (key) => {
  window.localStorage.removeItem(key);
};

export const clearStorage = () => {
  window.localStorage.clear();
};

export default {
  getStorageItem,
  setStorageItem,
  removeStorageItem,
  clearStorage,
};
