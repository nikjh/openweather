export const iconAttributes = (context, size, defaultAttrs = {}) => {
  const { width, height } = size;

  return {
    key: context.key,
    class: [
      context.data.class,
    ],
    style: [
      { pointerEvents: 'none' },
      context.data.style,
      context.data.staticStyle,
    ],
    attrs: {
      xmlns: 'http://www.w3.org/2000/svg',
      fill: 'currentColor',
      stroke: 'none',
      viewBox: `0 0 ${width} ${height}`,
      height: '1em',
      width: `${width / height}em`,
      ...defaultAttrs,
      ...context.data.attrs,
    },
  };
};

export default {
  iconAttributes,
};
