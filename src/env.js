export default {
  TOKEN: process.env.VUE_APP_TOKEN,
  GEODB_TOKEN: process.env.VUE_APP_GEODB_TOKEN,
};
