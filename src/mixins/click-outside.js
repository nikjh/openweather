const HANDLER_OPTION = 'onClickOutside';

const hasHandler = vm => typeof vm.$options[HANDLER_OPTION] === 'function';

export default {
  methods: {
    $_handleClickOutside(e) {
      if (!this.$el.contains(e.target)) {
        this.$options[HANDLER_OPTION].call(this);
      }
    },
  },
  mounted() {
    if (this.$isServer || !hasHandler(this)) {
      return;
    }

    document.addEventListener('click', this.$_handleClickOutside, true);
  },
  beforeDestroy() {
    if (this.$isServer || !hasHandler(this)) {
      return;
    }

    document.removeEventListener('click', this.$_handleClickOutside, true);
  },
};
