export const PROPS = {
  value: {
    type: String,
    default: '',
  },
  options: {
    type: Array,
    required: true,
    validator: (options) => {
      if (!options.length) {
        return false;
      }

      for (let i = 0; i < options.length; i += 1) {
        if (typeof options[i] !== 'object') {
          return false;
        }
        if (!options[i].value || !options[i].label) {
          return false;
        }
      }

      return true;
    },
  },
  required: {
    type: Boolean,
    default: false,
  },
  placeholder: {
    type: String,
    default: '',
  },
  resetText: {
    type: String,
    default: '',
  },
  fullwidth: {
    type: Boolean,
    default: false,
  },
  error: {
    type: Boolean,
    default: false,
  },
  dark: {
    type: Boolean,
    default: false,
  },
};

export const mainCssClass = 'app-select';

export default {
  PROPS,
  mainCssClass,
};
