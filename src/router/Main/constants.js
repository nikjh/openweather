import { firstCharToUpperCase } from '@/utils/strings';

export const UNITS = {
  metric: 'metric',
  imperial: 'imperial',
};

export const UNITS_OPTIONS = Object.keys(UNITS).map((value) => ({
  label: firstCharToUpperCase(value),
  value,
}));

export const UNITS_VALUES = {
  [UNITS.metric]: {
    temperature: '°C',
    pressure: 'hPa',
    windSpeed: 'm/s',
  },
  [UNITS.imperial]: {
    temperature: '°F',
    pressure: 'hPa',
    windSpeed: 'miles per hour',
  },
};

export default {
  UNITS,
  UNITS_OPTIONS,
  UNITS_VALUES,
};
