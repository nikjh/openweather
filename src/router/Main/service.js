import axios from '@/utils/axios';
import env from '@/env';

const searchCity = function serviceFindCity(params) {
  return axios.get('https://wft-geo-db.p.rapidapi.com/v1/geo/adminDivisions', {
    params,
    headers: {
      'x-rapidapi-host': 'wft-geo-db.p.rapidapi.com',
      'x-rapidapi-key': env.GEODB_TOKEN,
    },
  })
    .then((response) => response.data);
};

const getLocationWeather = function serviceGetCurrnetWeather(params) {
  return axios.get('https://api.openweathermap.org/data/2.5/weather', {
    params: {
      ...params,
      appid: env.TOKEN,
    },
  })
    .then((response) => response.data);
};

const getHourlyLocationWeather = function serviceGetHourlyWeather(params) {
  return axios.get('https://api.openweathermap.org/data/2.5/forecast', {
    params: {
      ...params,
      appid: env.TOKEN,
    },
  })
    .then((response) => response.data);
};

export default {
  searchCity,
  getLocationWeather,
  getHourlyLocationWeather,
};
