import { line as DrawLine, curveCardinal } from 'd3-shape';

export const d3Position = scale => d => Number(scale(d));

export const baseProps = {
  scale: {
    type: Function,
    required: true,
  },
  strokeColor: {
    type: String,
    default: '#aaaaaa',
  },
};

export const drawLine = (data, scale) => {
  const { x, y } = scale;
  return DrawLine()
    .x(d => x(d.x))
    .y(d => y(d.y))
    .curve(curveCardinal)(data);
};

export const pointCoordinates = (point, scale) => {
  const { x, y } = scale;
  return {
    cx: x(point.x || point.date),
    cy: y(point.y || point.temp),
  };
};

export const tooltipTemplate = (point, units) => `
<div>
  <p>Temperature: ${point.temp} ${units.temperature}</p>
  <p>Feels like: ${point.feels_like} ${units.temperature}</p>
  <p>Pressure: ${point.pressure} ${units.pressure}</p>
  <p>Humidity: ${point.humidity} %</p>
</div>
`;
