import { names, paths } from '@/constants/router';

export default [{
  path: paths.main,
  name: names.main,
  component: () => import(
    /* webpackPrefetch: true */
    /* webpackChunkName: 'main' */
    './index.vue'
  ),
}];
